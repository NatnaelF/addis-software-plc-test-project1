<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   
	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php wp_bootstrap_starter_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->
    
    
    
<div class = "row" style="display:flex;">
    
           <div class= "col-sm-6" >
               
                <?php
                 echo "<img src='".get_field("image")."' >";
                ?>
               
               </div>
        
        
   <div class="col-sm-6" >
            
		   <?php
        
        
                if ( is_single() ) :

                            the_content();



                else :
                    the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );

                endif;


                    wp_link_pages( array(
                        'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
                        'after'  => '</div>',
                    ) );
		     ?>
           
            <?php
               
                echo "<p>The Price is &nbsp";
                echo get_field("Price");
               
             ?>
               
            <div class = "row_bottom" style="display:flex;">
        

         
                    <div class="col-sm-6 bg-secondary" style="display:flex;">

                          <div>

                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" class="bi bi-check2-square" viewBox="0 0 16 16">
                                 <path d="M3 14.5A1.5 1.5 0 0 1 1.5 13V3A1.5 1.5 0 0 1 3 1.5h8a.5.5 0 0 1 0 1H3a.5.5 0 0 0-.5.5v10a.5.5 0 0 0 .5.5h10a.5.5 0 0 0 .5-.5V8a.5.5 0 0 1 1 0v5a1.5 1.5 0 0 1-1.5 1.5H3z"/>
                                  <path d="m8.354 10.354 7-7a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0z"/>
                                </svg>


                        </div>
                        
                        <div class=" col-sm-1">

                        </div>
                        
                        <div >
                             <?php

                                echo get_field("facts");
                                ?>

                        </div>



                     </div>
                
                     <div class="col-md-1 col-md-offset-1">
                
                     </div>
                
                     <div class="col-sm-6 bg-info" style="display:flex">
                         
                                <div>
                                    
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-cash" viewBox="0 0 16 16">
                                              <path d="M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/>
                                              <path d="M0 4a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4zm3 0a2 2 0 0 1-2 2v4a2 2 0 0 1 2 2h10a2 2 0 0 1 2-2V6a2 2 0 0 1-2-2H3z"/>
                                            </svg>
                                    
                                </div>
                         
                                <div class = "col-sm-1">

                                </div>

                                <div>

                                    <?php
                                        echo get_field("payment_plan");
                                        ?>

                                </div>

    
    
                      </div>
   
            </div>
   </div>
	
</div>
    
    
        
    
    
     
    

	<footer class="entry-footer">
		<?php wp_bootstrap_starter_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
